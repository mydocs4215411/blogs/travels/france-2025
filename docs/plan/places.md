
## 1일차 (2016-05-16)

### [Auvers sur Oise](https://en.wikipedia.org/wiki/Auvers-sur-Oise)

## 2일차 (2016-05-17)

### [Les Jardins de Monet à Giverny](https://giverny.org/gardens/fcm/visitgb.htm)

### [Château Gaillard](https://en.normandie-tourisme.fr/hidden-gems/chateau-gaillard/)

### [Rouen](https://en.visiterouen.com/inspirations/visit-in-1-day/)


## 3일차 (2016-05-18)

### [Rouen](https://en.visiterouen.com/inspirations/visit-in-1-day/)

### [Fécamp](https://en.normandie-tourisme.fr/unmissable-sites/fecamp/)

### [Étretat](https://en.normandie-tourisme.fr/unmissable-sites/etretat/)


## 17일차 (2016-06-01)

### [Avignon]

### [Saint Remy de Provence - Town in the Alpilles](https://www.mairie-saintremydeprovence.com/)

### [Gordes](https://www.avignon-et-provence.com/en/tourism-provence/gordes)

### [Roussillon](https://www.onlyprovence.com/roussillon-france/)

## 18~20일차 (2016-06-02~04), 

### [Salon de Provence]()

### [Aix en Provence]()

### [Marseille]()

### [Cassis]()

### [Gassin]()

### [St. Tropez]()

### [Prejus]()

